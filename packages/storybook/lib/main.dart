import 'package:flutter/material.dart';
import 'package:kickandflutter/campaigns/add_campaign_view.dart';
import 'package:kickandflutter/campaigns/campaign.dart';
import 'package:kickandflutter/campaigns/campaign_log_entry.dart';
import 'package:kickandflutter/campaigns/campaign_status.dart';
import 'package:kickandflutter/campaigns/campaigns_view.dart';
import 'package:kickandflutter/campaigns/campaigns_widget.dart';
import 'package:storybook_flutter/storybook_flutter.dart';

void main() {
  runApp(Storybook(
    initialStory: 'widget/campaigns',
    stories: [
      Story(
        name: 'widget/campaigns',
        builder: (context) {
          return CampaignsWidget(
            campaigns: [
              Campaign(
                  name: 'name',
                  status: context.knobs.options<CampaignStatus>(
                      label: 'status',
                      initial: CampaignStatus.open,
                      options: CampaignStatus.values
                          .map(
                            (e) => Option(label: e.name, value: e),
                          )
                          .toList(growable: false)),
                  lastUpdate: CampaignLogEntry(content: context.knobs.text(label: 'last Update')),
                  platform: '',
                  urlCampaign: ''),
              Campaign(
                  name: 'Otro name',
                  status: context.knobs.options<CampaignStatus>(
                      label: 'status',
                      initial: CampaignStatus.open,
                      options: CampaignStatus.values
                          .map(
                            (e) => Option(label: e.name, value: e),
                          )
                          .toList(growable: false)),
                  lastUpdate: null,
                  platform: '',
                  urlCampaign: '')
            ],
            onAddCampaignLogEntry: (Campaign campaign, String logEntryText) {},
          );
        },
      ),
      Story(
          name: 'views/CampaignsView',
          builder: (context) {
            return CampaignsViewWidget(
                noActiveCampaigns: context.knobs.boolean(label: 'no se que significa esto'),
                activeCampaigns: [
                  Campaign(
                    name: 'pruebaDesacople',
                    status: CampaignStatus.waitingForRewards,
                    lastUpdate: null,
                    platform: '',
                    urlCampaign: '',
                  ),
                ],
                goToAddCampaign: (BuildContext context) {});
          }),
      Story(
          name: 'widget/addCampaign',
          builder: (context) {
            return AddCampaignWidget(
                onUpDateCampaignName: (String string) {},
                name: 'name',
                onUpdateCampaignPlatform: (String string) {},
                platform: 'platform',
                onUpdateCampaignUrl: (String string) {},
                url: 'url',
                onUpdateCampaignStatus: (CampaignStatus campaignStatus) {},
                status: CampaignStatus.open,
                saveCampaign: (BuildContext context) {});
          }),
    ],
  ));
}
