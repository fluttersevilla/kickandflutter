import 'package:kickandflutter/application/add_campaign_log_entry_use_case.dart';
import 'package:kickandflutter/application/fetch_active_campaigns_use_case.dart';
import 'package:kickandflutter/campaigns/campaign.dart';
import 'package:kickandflutter/campaigns/infrastructure/campaigns_cubit.dart';

class CampaignsCubitTestBuilder {
  static CampaignsCubit build({required List<Campaign> campaignsRepository}) => CampaignsCubit(
        fetchActiveCampaignsUseCase: FetchActiveCampaignsUseCase(campaignRepository: campaignsRepository),
        addCampaignLogEntryUseCase: AddCampaignLogEntryUseCase(campaignRepository: campaignsRepository),
      );
}
