import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:kickandflutter/campaigns/campaigns_page.dart';

import 'campaign_test_builder.dart';
import 'campaigns_cubit_test_builder.dart';

void main() {
  testWidgets(
    'should suggest add a campaign if there are none',
    (widgetTester) async {
      //given
      final materialApp = MaterialApp(
        home: CampaignsPage(
          create: () => CampaignsCubitTestBuilder.build(campaignsRepository: []),
        ),
      );

      //when
      await widgetTester.pumpWidget(materialApp);

      //then
      expect(find.byType(ElevatedButton), findsOne);
    },
  );

  testWidgets(
    'should display an active campaign',
    (widgetTester) async {
      //given
      final materialApp = MaterialApp(
        home: CampaignsPage(
          create: () => CampaignsCubitTestBuilder.build(campaignsRepository: [CampaignTestBuilder.openedCampaign(name: 'name')]),
        ),
      );

      //when
      await widgetTester.pumpWidget(materialApp);

      //then
      expect(find.byType(ListView), findsOne);
    },
  );

  testWidgets(
    'should add an update to an active campaign',
    (widgetTester) async {
      //given
      const expectedLastUpdate = 'Jorge me debe 24 euros';

      final materialApp = MaterialApp(
        home: CampaignsPage(
          create: () => CampaignsCubitTestBuilder.build(campaignsRepository: [CampaignTestBuilder.openedCampaign(name: 'name')]),
        ),
      );

      await widgetTester.pumpWidget(materialApp);

      //when
      await widgetTester.tap(find.byKey(const Key('buttonUpdate')));
      // await widgetTester.tap(find.byType(Icon));
      await widgetTester.pumpAndSettle();
      await widgetTester.enterText(find.byKey(const Key('updateTextField')), expectedLastUpdate);
      await widgetTester.tap(find.byKey(const Key('sendUpdate')));
      await widgetTester.pumpAndSettle();

      //then
      expect(find.text(expectedLastUpdate), findsOne);
    },
  );
}
