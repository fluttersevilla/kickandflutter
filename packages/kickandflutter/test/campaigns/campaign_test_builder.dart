import 'package:kickandflutter/campaigns/campaign.dart';
import 'package:kickandflutter/campaigns/campaign_log_entry.dart';
import 'package:kickandflutter/campaigns/campaign_status.dart';

class CampaignTestBuilder {
  static Campaign openedCampaign({required String name, String? lastUpdate}) => Campaign(
        name: name,
        platform: 'KickStarter',
        urlCampaign: 'www.Kaf.com',
        status: CampaignStatus.open,
        lastUpdate: lastUpdate == null ? null : CampaignLogEntry(content: lastUpdate),
      );
}
