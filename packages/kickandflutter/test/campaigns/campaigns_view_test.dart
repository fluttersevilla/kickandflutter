import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:kickandflutter/campaigns/add_campaign_page.dart';
import 'package:kickandflutter/campaigns/campaign.dart';
import 'package:kickandflutter/campaigns/campaigns_view.dart';
import 'package:kickandflutter/campaigns/infrastructure/campaigns_cubit.dart';

import 'campaign_test_builder.dart';
import 'campaigns_cubit_test_builder.dart';

void main() {
  testWidgets('should suggest adding a new campaign if there are no active campaigns', (widgetTester) async {
    //given
    await widgetTester.pumpWidget(
      MaterialApp(
          home: BlocProvider(
        create: (context) => CampaignsCubitTestBuilder.build(campaignsRepository: [])..fetchActiveCampaigns(),
        child: const CampaignsView(),
      )),
    );

    //expect
    expect(find.byType(ElevatedButton), findsOne);
  });

  testWidgets('should receive the active campaigns list', (widgetTester) async {
    //given
    const expectedCampaignName = 'esta es ejor';
    final expectedCampaigns = <Campaign>[CampaignTestBuilder.openedCampaign(name: expectedCampaignName)];

    //when
    await widgetTester.pumpWidget(
      MaterialApp(
        home: BlocProvider<CampaignsCubit>(
          create: (context) {
            return CampaignsCubitTestBuilder.build(campaignsRepository: expectedCampaigns)..fetchActiveCampaigns();
          },
          child: BlocBuilder<CampaignsCubit, CampaignsState>(
            builder: (context, state) {
              return const CampaignsView();
            },
          ),
        ),
      ),
    );

    //then
    expect(find.text(expectedCampaignName), findsOne);
  });

  testWidgets('should display new update to campaign', (widgetTester) async {
    //given
    const expectedCampaignName = 'esta es ejor';
    const expecteedLastUpdateText = 'I need this ';
    final givenCampaign = CampaignTestBuilder.openedCampaign(name: expectedCampaignName);

    final campaignsCubit = CampaignsCubitTestBuilder.build(campaignsRepository: <Campaign>[givenCampaign]);

    await widgetTester.pumpWidget(
      MaterialApp(
        home: BlocProvider<CampaignsCubit>(
          create: (context) {
            return campaignsCubit..fetchActiveCampaigns();
          },
          child: BlocBuilder<CampaignsCubit, CampaignsState>(
            builder: (context, state) {
              return const CampaignsView();
            },
          ),
        ),
      ),
    );

    //when
    campaignsCubit.addCampaignLogEntry(expecteedLastUpdateText, givenCampaign);
    await widgetTester.pumpAndSettle();

    //then
    expect(find.text(expecteedLastUpdateText), findsOne);
  });

  testWidgets('should receive two different campaigns', (widgetTester) async {
    //given
    final givenCampaigns = <Campaign>[
      CampaignTestBuilder.openedCampaign(name: 'Campaign1'),
      CampaignTestBuilder.openedCampaign(name: 'Campaign2'),
    ];

    final app = MaterialApp(
      home: BlocProvider<CampaignsCubit>(
        create: (context) {
          return CampaignsCubitTestBuilder.build(campaignsRepository: givenCampaigns)..fetchActiveCampaigns();
        },
        child: const CampaignsView(),
      ),
    );

    //when
    await widgetTester.pumpWidget(app);

    //then
    expect(find.text('Campaign1'), findsOne);
    expect(find.text('Campaign2'), findsOne);
  });

  testWidgets('Should check that there is a fab IF there is at least one campaign', (widgetTester) async {
    //given
    final givenCampaigns = <Campaign>[
      CampaignTestBuilder.openedCampaign(name: 'Campaign1'),
      CampaignTestBuilder.openedCampaign(name: 'Campaign2'),
    ];
    await widgetTester.pumpWidget(
      MaterialApp(
        home: BlocProvider(
          create: (context) => CampaignsCubitTestBuilder.build(campaignsRepository: givenCampaigns)..fetchActiveCampaigns(),
          child: const CampaignsView(),
        ),
      ),
    );

    //expect
    expect(find.byType(FloatingActionButton), findsOne);
  });

  testWidgets('should show add campaign page', (widgetTester) async {
    //given

    await widgetTester.pumpWidget(
      MaterialApp(
          home: BlocProvider(
            create: (context) => CampaignsCubitTestBuilder.build(campaignsRepository: [])..fetchActiveCampaigns(),
            child: const CampaignsView(),
          )),
    );

    await widgetTester.tap(find.byType(ElevatedButton));
    await widgetTester.pumpAndSettle();

    //then
    expect(find.byType(AddCampaignPage), findsOne);
    expect(find.text('Esto es la página para añadir campañas'), findsOne);
  });
}
