import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:kickandflutter/campaigns/campaign.dart';
import 'package:kickandflutter/campaigns/campaign_status.dart';
import 'package:kickandflutter/campaigns/campaigns_widget.dart';

import 'campaign_test_builder.dart';

void main() {
  testWidgets(
    'should display all the relevant info',
    (widgetTester) async {
      //given
      const expectedCampaignName = 'turbo juego';
      final expectedStatusName = CampaignStatus.open.name;

      final expectedOpenedCampaign = CampaignTestBuilder.openedCampaign(name: expectedCampaignName, lastUpdate: 'aduanas!!!!');
      final materialApp = MaterialApp(
        home: Scaffold(
          body: CampaignsWidget(campaigns: [expectedOpenedCampaign], onAddCampaignLogEntry: (Campaign campaign, String logEntryText) {  },),
        ),
      );

      //when
      await widgetTester.pumpWidget(materialApp);

      //then
      expect(find.text(expectedCampaignName), findsOne);
      expect(find.text(expectedStatusName), findsOne);
      expect(find.byKey( const Key('buttonUpdate')), findsOne);
      expect(find.text(expectedOpenedCampaign.getLastUpdate()), findsOne);
    },
  );
}
