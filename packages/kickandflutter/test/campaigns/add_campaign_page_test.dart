import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:kickandflutter/campaigns/add_campaign_page.dart';
import 'package:kickandflutter/campaigns/campaign.dart';

import 'campaign_test_builder.dart';

void main() {
  testWidgets('should display elements to be able to add a campaign name', (widgetTester) async {
    //given

    await widgetTester.pumpWidget(const MaterialApp(
        home: AddCampaignPage(
      campaignRepository: [],
    )));

    //when
    await widgetTester.pumpAndSettle();

    //then
    expect(find.byType(TextFormField), findsAtLeast(1));
    expect(find.byKey(const Key('createCampaignKey')), findsOne);
  });

  testWidgets('should create a campaign with provided parameters', (widgetTester) async {
    //given
    final List<Campaign> campaignsRepository = [];
    final Campaign expectedCampaign = CampaignTestBuilder.openedCampaign(name: 'Cardboards very expensive');

    await widgetTester.pumpWidget(MaterialApp(home: AddCampaignPage(campaignRepository: campaignsRepository)));
    await widgetTester.pumpAndSettle();


    //when
    await widgetTester.enterText(find.byKey(const Key('campaignNameField')), expectedCampaign.getName());
    await widgetTester.enterText(find.byKey(const Key('campaignPlatformField')), expectedCampaign.getPlatform());
    await widgetTester.enterText(find.byKey(const Key('campaignUrlField')), expectedCampaign.getUrlCampaign());

    await widgetTester.tap(find.byKey(const Key('campaignStatusField')));
    await widgetTester.pumpAndSettle();
    await widgetTester.tap(find.text(expectedCampaign.getCurrentStatusName()).last);
    await widgetTester.pumpAndSettle();

    await widgetTester.tap(find.byKey(const Key('createCampaignKey')));
    await widgetTester.pumpAndSettle();

    //then
    expect(campaignsRepository.first.getName(), expectedCampaign.getName());
    expect(campaignsRepository.first.getPlatform(), expectedCampaign.getPlatform());
    expect(campaignsRepository.first.getUrlCampaign(), expectedCampaign.getUrlCampaign());
    expect(campaignsRepository.first.getCurrentStatusName(), expectedCampaign.getCurrentStatusName());
  });
}
