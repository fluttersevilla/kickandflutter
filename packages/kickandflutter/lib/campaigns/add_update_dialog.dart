import 'package:flutter/material.dart';

class AddUpdateDialog extends StatelessWidget {
  final _textController = TextEditingController();

  AddUpdateDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const Text('addUpdateTitlKey'),
          TextFormField(key: const Key('updateTextField'), controller: _textController,),
          ElevatedButton(key: const Key('sendUpdate'), onPressed: () => Navigator.of(context).pop(_textController.text), child: const Text('sendUpdateKey')),
          ElevatedButton(onPressed: () => Navigator.of(context).pop(null), child: const Text('cancelUpdateKey')),
        ],
      ),
    );
  }
}
