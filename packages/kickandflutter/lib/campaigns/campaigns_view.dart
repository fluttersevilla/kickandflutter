import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kickandflutter/campaigns/add_campaign_page.dart';
import 'package:kickandflutter/campaigns/campaign.dart';
import 'package:kickandflutter/campaigns/campaigns_widget.dart';
import 'package:kickandflutter/campaigns/infrastructure/campaigns_cubit.dart';

part 'campaigns_view_widget.dart';

class CampaignsView extends StatelessWidget {

  const CampaignsView({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CampaignsCubit, CampaignsState>(
      builder: (context, state) {
        return CampaignsViewWidget(
          noActiveCampaigns: state.noActiveCampaigns(),
          activeCampaigns: state.getActiveCampaigns(),
          goToAddCampaign: (BuildContext context) {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => AddCampaignPage(campaignRepository: state.getActiveCampaigns()),
              ),
            );
          },
        );
      },
    );
  }
}
