class CampaignLogEntry {
  final String _content;

  CampaignLogEntry({required String content}) : _content = content;

  String getContent() => _content;
}