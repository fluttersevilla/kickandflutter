import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kickandflutter/campaigns/campaigns_view.dart';
import 'package:kickandflutter/campaigns/infrastructure/campaigns_cubit.dart';

class CampaignsPage extends StatelessWidget {
  final CampaignsCubit Function() _create;

  const CampaignsPage({super.key, required CampaignsCubit Function() create}) : _create = create;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CampaignsCubit>(
      create: (context) => _create()..fetchActiveCampaigns(),
      child: const CampaignsView(),
    );
  }
}
