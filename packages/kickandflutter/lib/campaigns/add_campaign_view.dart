import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kickandflutter/campaigns/campaign_status.dart';
import 'package:kickandflutter/campaigns/infrastructure/add_campaign_cubit.dart';

part 'add_campaign_widget.dart';

class AddCampaignView extends StatelessWidget {
  const AddCampaignView({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AddCampaignCubit, AddCampaignState>(
      builder: (BuildContext context, AddCampaignState state) {
        return AddCampaignWidget(
          name: context.read<AddCampaignCubit>().state.getCampaignName(),
          onUpDateCampaignName: (name) {
            context.read<AddCampaignCubit>().updateCampaignName(name);
          },
          platform: context.read<AddCampaignCubit>().state.getCampaignPlatform(),
          onUpdateCampaignPlatform: (platform) {
            context.read<AddCampaignCubit>().updateCampaignPlatform(platform);
          },
          url: context.read<AddCampaignCubit>().state.getCampaignUrl(),
          onUpdateCampaignUrl: (url) {
            context.read<AddCampaignCubit>().updateCampaignUrlCampaign(url);
          },
          status: context.read<AddCampaignCubit>().state.getCampaignStatus(),
          onUpdateCampaignStatus: (status) {
            context.read<AddCampaignCubit>().updateCampaignStatus(status);
          },
          saveCampaign: (context) {
            context.read<AddCampaignCubit>().saveCampaign();
          },
        );
      },
    );
  }
}
