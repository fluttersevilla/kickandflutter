part of 'campaigns_view.dart';

class CampaignsViewWidget extends StatelessWidget {
  final bool _noActiveCampaigns;
  final List<Campaign> _activeCampaigns;
  final Function(BuildContext) _goToAddCampaign;

  const CampaignsViewWidget({
    required bool noActiveCampaigns,
    required List<Campaign> activeCampaigns,
    required Function(BuildContext) goToAddCampaign,
    super.key })
      : _noActiveCampaigns = noActiveCampaigns,
        _activeCampaigns = activeCampaigns,
        _goToAddCampaign = goToAddCampaign;


@override
Widget build(BuildContext context) {
  return Scaffold(
    body: _noActiveCampaigns
        ? ElevatedButton(onPressed: () => _goToAddCampaign(context), child: const Text('Quillo no hay na, gastate el dinero que los de kickstarter tienen que comer!!!'))
        : CampaignsWidget(
      campaigns: _activeCampaigns,
      onAddCampaignLogEntry: (Campaign campaign, String logEntryText) => context.read<CampaignsCubit>().addCampaignLogEntry(logEntryText, campaign),
    ),
    floatingActionButton: _noActiveCampaigns ? null : FloatingActionButton(onPressed: () => _goToAddCampaign(context)),
  );
}}
