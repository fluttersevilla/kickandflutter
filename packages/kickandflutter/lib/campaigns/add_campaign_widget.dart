part of 'add_campaign_view.dart';

class AddCampaignWidget extends StatelessWidget {
  final void Function(String campaignName) _onUpdateCampaignName;
  final String _name;
  final void Function(String campaignName) _onUpdateCampaignPlatform;
  final String _platform;
  final void Function(String campaignName) _onUpdateCampaignUrl;
  final String _url;
  final void Function(CampaignStatus campaignStatus) _onUpdateCampaignStatus;
  final CampaignStatus _status;
  final void Function(BuildContext context) _saveCampaign;

  const AddCampaignWidget({
    super.key,
    required void Function(String name) onUpDateCampaignName,
    required String name,
    required void Function(String platform) onUpdateCampaignPlatform,
    required String platform,
    required void Function(String url) onUpdateCampaignUrl,
    required String url,
    required void Function(CampaignStatus campaignStatus) onUpdateCampaignStatus,
    required CampaignStatus status,
    required void Function(BuildContext context) saveCampaign,
  })  : _onUpdateCampaignName = onUpDateCampaignName,
        _name = name,
        _onUpdateCampaignPlatform = onUpdateCampaignPlatform,
        _platform = platform,
        _onUpdateCampaignUrl = onUpdateCampaignUrl,
        _url = url,
        _onUpdateCampaignStatus = onUpdateCampaignStatus,
        _status = status,
        _saveCampaign = saveCampaign;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Campaign'),
      ),
      body: Column(
        children: [
          const Text('Esto es la página para añadir campañas'),
          TextFormField(
            key: const Key('campaignNameField'),
            initialValue: _name,
            onChanged: (value) {
              _onUpdateCampaignName(value);
            },
          ),
          TextFormField(
              key: const Key('campaignPlatformField'),
              initialValue: _platform,
              onChanged: (value) {
                _onUpdateCampaignPlatform(value);
              }),
          TextFormField(
              key: const Key('campaignUrlField'),
              initialValue: _url,
              onChanged: (value) {
                _onUpdateCampaignUrl(value);
              }),
          DropdownButtonFormField(
              key: const Key('campaignStatusField'),
              value: _status,
              items: CampaignStatus.values.map((status) {
                return DropdownMenuItem<CampaignStatus>(
                  value: status,
                  child: Text(status.name),
                );
              }).toList(),
              onChanged: (newStatus) {
                if (newStatus != null) {
                  _onUpdateCampaignStatus(newStatus);
                }
              }),
          ElevatedButton(
              key: const Key('createCampaignKey'),
              onPressed: () {
                _saveCampaign(context);
              },
              child: const Text('CreateCampaign'))
        ],
      ),
    );
  }
}
