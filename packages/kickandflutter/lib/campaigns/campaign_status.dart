enum CampaignStatus {
  pre,
  open,
  waitingForPledge,
  pledgeOpened,
  waitingForRewards,
  close
}
