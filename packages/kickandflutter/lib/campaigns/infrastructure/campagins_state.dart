part of 'campaigns_cubit.dart';

class CampaignsState {
  final List<Campaign> _campaigns;

  factory CampaignsState.initial() => CampaignsState(campaigns: []);

  CampaignsState({required List<Campaign> campaigns}) : _campaigns = campaigns;

  List<Campaign> getActiveCampaigns() {
    return _campaigns;
  }

  CampaignsState fetchedCampaigns({required List<Campaign> campaigns}) {
    return copyWith(newCampaigns: campaigns);
  }

  CampaignsState copyWith({List<Campaign>? newCampaigns}) {
    return CampaignsState(campaigns: newCampaigns ?? _campaigns);
  }

  bool noActiveCampaigns() => _campaigns.isEmpty;

  int getNumberOfActiveCampaigns() => _campaigns.length;
}
