import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kickandflutter/application/add_campaign_log_entry_use_case.dart';
import 'package:kickandflutter/application/fetch_active_campaigns_use_case.dart';
import 'package:kickandflutter/campaigns/campaign.dart';

part 'campagins_state.dart';

class CampaignsCubit extends Cubit<CampaignsState> {
  final FetchActiveCampaignsUseCase _fetchActiveCampaignsUseCase;
  final AddCampaignLogEntryUseCase _addCampaignLogEntryUseCase;

  CampaignsCubit({required FetchActiveCampaignsUseCase fetchActiveCampaignsUseCase, required AddCampaignLogEntryUseCase addCampaignLogEntryUseCase})
      : _fetchActiveCampaignsUseCase = fetchActiveCampaignsUseCase,
        _addCampaignLogEntryUseCase = addCampaignLogEntryUseCase,
        super(CampaignsState.initial());

  void fetchActiveCampaigns() {
    final fetchedCampaigns = _fetchActiveCampaignsUseCase.execute();
    emit(state.fetchedCampaigns(campaigns: fetchedCampaigns));
  }

  void addCampaignLogEntry(String logEntryText, Campaign campaign) {
    _addCampaignLogEntryUseCase.execute(campaign, logEntryText);
    fetchActiveCampaigns();
  }
}
