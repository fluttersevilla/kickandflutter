import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kickandflutter/application/add_campaign_use_case.dart';
import 'package:kickandflutter/campaigns/campaign.dart';
import 'package:kickandflutter/campaigns/campaign_status.dart';

part 'add_campaign_state.dart';

class AddCampaignCubit extends Cubit<AddCampaignState> {
  final AddCampaignUseCase _addCampaignUseCase;

  AddCampaignCubit({required AddCampaignUseCase addCampaignUseCase})
      : _addCampaignUseCase = addCampaignUseCase,
        super(AddCampaignState.initialState());

  void updateCampaignStatus(CampaignStatus newStatus) {
    emit(state.updateCampaignStatus(campaignStatus: newStatus));
  }

  void updateCampaignName(String newName) {
    emit(state.updateCampaignName(newName: newName));
  }

  void updateCampaignPlatform(String newPlatform) {
    emit(state.updateCampaignPlatform(newPlatform: newPlatform));
  }

  void updateCampaignUrlCampaign(String newUrl) {
    emit(state.updateCampaignUrl(newUrl: newUrl));
  }

  void saveCampaign() {
    Campaign newCampaign = Campaign(name: state._name, platform: state._platform, urlCampaign: state._url, status: state._status, lastUpdate: null);
    _addCampaignUseCase.execute(newCampaign);
  }
}
