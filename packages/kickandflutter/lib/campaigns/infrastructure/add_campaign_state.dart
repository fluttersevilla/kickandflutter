part of 'add_campaign_cubit.dart';

class AddCampaignState {
  final String _name;
  final String _platform;
  final String _url;
  final CampaignStatus _status;

  AddCampaignState({required CampaignStatus currentCampaignStatus, required String name, required String platform, required String url})
      : _status = currentCampaignStatus,
        _name = name,
        _platform = platform,
        _url = url;

  factory AddCampaignState.initialState() => AddCampaignState(name: '', platform: '', url: '', currentCampaignStatus: CampaignStatus.open);

  AddCampaignState _copyWith({String? name, String? platform, String? url, CampaignStatus? campaignStatus}) {
    return AddCampaignState(
      name: name ?? _name,
      platform: platform ?? _platform,
      url: url ?? _url,
      currentCampaignStatus: campaignStatus ?? _status,
    );
  }

  AddCampaignState updateCampaignName({required String newName}) {
    return _copyWith(name: newName);
  }

  AddCampaignState updateCampaignPlatform({required String newPlatform}) {
    return _copyWith(platform: newPlatform);
  }

  AddCampaignState updateCampaignUrl({required String newUrl}) {
    return _copyWith(url: newUrl);
  }

  AddCampaignState updateCampaignStatus({required CampaignStatus campaignStatus}) {
    return _copyWith(campaignStatus: campaignStatus);
  }

  String getCampaignName() => _name;

  String getCampaignPlatform() => _platform;

  String getCampaignUrl() => _url;

  CampaignStatus getCampaignStatus() => _status;
}
