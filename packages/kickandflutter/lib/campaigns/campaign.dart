import 'package:kickandflutter/campaigns/campaign_log_entry.dart';
import 'package:kickandflutter/campaigns/campaign_status.dart';

class Campaign {
  final String _name;
  final String _platform;
  final String _urlCampaign;
  final CampaignStatus _status;

  final CampaignLogEntry? _lastUpdate;

  Campaign({required String name, required String platform, required String urlCampaign, required CampaignStatus status, required CampaignLogEntry? lastUpdate})
      : _name = name,
        _platform = platform,
        _urlCampaign = urlCampaign,
        _status = status,
        _lastUpdate = lastUpdate;

  String getName() => _name;

  String getPlatform() => _platform;

  String getUrlCampaign() => _urlCampaign;

  String getCurrentStatusName() => _status.name;

  String getLastUpdate() => _lastUpdate?.getContent() ?? '';

  CampaignStatus getStatus() => _status;

  Campaign addEntryLog(CampaignLogEntry campaignLogEntry) => Campaign(
        name: _name,
        status: _status,
        lastUpdate: campaignLogEntry,
        platform: _platform,
        urlCampaign: _urlCampaign,
      );
}
