import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kickandflutter/application/add_campaign_use_case.dart';
import 'package:kickandflutter/campaigns/add_campaign_view.dart';
import 'package:kickandflutter/campaigns/campaign.dart';
import 'package:kickandflutter/campaigns/infrastructure/add_campaign_cubit.dart';

class AddCampaignPage extends StatelessWidget {
  final List<Campaign> _campaignRepository;

  const AddCampaignPage({super.key, required List<Campaign> campaignRepository}) : _campaignRepository = campaignRepository;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AddCampaignCubit>(
      create: (BuildContext context) {
        return AddCampaignCubit(
          addCampaignUseCase: AddCampaignUseCase(campaignRepository: _campaignRepository),
        );
      },
      child: const AddCampaignView(),
    );
  }
}
