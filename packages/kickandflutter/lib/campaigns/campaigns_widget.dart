import 'package:flutter/material.dart';
import 'package:kickandflutter/campaigns/add_update_dialog.dart';
import 'package:kickandflutter/campaigns/campaign.dart';

class CampaignsWidget extends StatelessWidget {
  final List<Campaign> _campaigns;

  final Function(Campaign campaign, String logEntryText) _onAddCampaignLogEntry;

  const CampaignsWidget({super.key, required List<Campaign> campaigns, required Function(Campaign campaign, String logEntryText) onAddCampaignLogEntry})
      : _campaigns = campaigns,
        _onAddCampaignLogEntry = onAddCampaignLogEntry;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(10),
      itemCount: _campaigns.length,
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListTile(
          title: Text(
            _campaigns[index].getName(),
            style: const TextStyle(
              fontSize: 25,fontWeight: FontWeight.bold
            ),
          ),
          tileColor: Colors.white,
          shape: RoundedRectangleBorder(
            side: const BorderSide(color: Color.fromARGB(255, 5, 206, 120), width: 2),
            borderRadius: BorderRadius.circular(20),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(_campaigns[index].getCurrentStatusName()),
              Text(_campaigns[index].getLastUpdate()),
              ElevatedButton.icon(key: const Key('buttonUpdate'),
                icon: const Icon(
                  Icons.note_add,
                  color: Color.fromARGB(255, 5, 206, 120),
                ),
                label: const Text(
                  'Update',
                  style: TextStyle(color: Colors.black),
                ),
                onPressed: () {
                  showGeneralDialog<String?>(
                    context: context,
                    pageBuilder: (context, animation, secondaryAnimation) {
                      return AddUpdateDialog();
                    },
                  ).then(
                    (entryLogText) {
                      if (entryLogText != null) {
                        _onAddCampaignLogEntry(_campaigns[index], entryLogText);
                      }
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
