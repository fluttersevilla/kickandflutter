import 'package:kickandflutter/campaigns/campaign.dart';
import 'package:kickandflutter/campaigns/campaign_log_entry.dart';

class AddCampaignLogEntryUseCase {
  final List<Campaign> _campaignRepository;

  //TODO: add time repo

  AddCampaignLogEntryUseCase({required List<Campaign> campaignRepository}) : _campaignRepository = campaignRepository;

  void execute(Campaign campaign, String logEntryText) {
    var indexOf = _campaignRepository.indexOf(campaign);
    _campaignRepository[indexOf] = campaign.addEntryLog(CampaignLogEntry(content: logEntryText));
  }
}
