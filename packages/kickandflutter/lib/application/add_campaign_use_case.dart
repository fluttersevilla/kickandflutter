import 'package:kickandflutter/campaigns/campaign.dart';

class AddCampaignUseCase {
   final List<Campaign> _campaignRepository;

  AddCampaignUseCase({required List<Campaign> campaignRepository}) : _campaignRepository = campaignRepository;

  void execute(Campaign newCampaign) {
    _campaignRepository.add(newCampaign);
  }
}
