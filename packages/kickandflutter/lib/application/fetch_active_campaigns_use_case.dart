import 'package:kickandflutter/campaigns/campaign.dart';

class FetchActiveCampaignsUseCase {
  final List<Campaign> _campaignRepository;

  FetchActiveCampaignsUseCase({required List<Campaign> campaignRepository}) : _campaignRepository = campaignRepository;

  List<Campaign> execute() {
    return _campaignRepository;
  }
}
