import 'package:flutter/material.dart';
import 'package:kickandflutter/application/add_campaign_log_entry_use_case.dart';
import 'package:kickandflutter/application/fetch_active_campaigns_use_case.dart';
import 'package:kickandflutter/campaigns/campaign.dart';
import 'package:kickandflutter/campaigns/campaign_status.dart';
import 'package:kickandflutter/campaigns/campaigns_page.dart';
import 'package:kickandflutter/campaigns/infrastructure/campaigns_cubit.dart';

class KickAndFlutterApp extends StatelessWidget {
  const KickAndFlutterApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final temporaryRepository = [Campaign(name: 'no te lo crees ni tu',platform: 'KickAndRun',urlCampaign: "www.aspiradorasconcabeza.com", status: CampaignStatus.open, lastUpdate: null)];
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // TRY THIS: Try running your application with "flutter run". You'll see
        // the application has a purple toolbar. Then, without quitting the app,
        // try changing the seedColor in the colorScheme below to Colors.green
        // and then invoke "hot reload" (save your changes or press the "hot
        // reload" button in a Flutter-supported IDE, or press "r" if you used
        // the command line to start the app).
        //
        // Notice that the counter didn't reset back to zero; the application
        // state is not lost during the reload. To reset the state, use hot
        // restart instead.
        //
        // This works for code too, not just values: Most code changes can be
        // tested with just a hot reload.
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: CampaignsPage(
        create: () => CampaignsCubit(
          fetchActiveCampaignsUseCase: FetchActiveCampaignsUseCase(campaignRepository: temporaryRepository),
          addCampaignLogEntryUseCase: AddCampaignLogEntryUseCase(campaignRepository: temporaryRepository),
        ),
      ),
    );
  }
}
