# KickAndFlutter

Deja de usar hojas de cálculos, post-its o aplicaciones de mensajería para registrar el estado de tus [Kickstarters](https://en.wikipedia.org/wiki/Kickstarter).  


## Description
Aplicación desarrollada por la comunidad de Flutter Sevilla, hecha en Flutter para gestionar las campañas de juegos de mesa en las que participas en las diferentes [plataformas de micromecenazgos](https://en.wikipedia.org/wiki/Kickstarter).
Vamos a usar un monorepo.

## Badges
![Estado del proyecto](https://gitlab.com/fluttersevilla/kickandflutter/badges/main/pipeline.svg)

## Visuals
TODO

## Installation
Necesitas tener instalado:
* Flutter
* [FVM](https://pub.dev/packages/fvm/)
* [Melos](https://pub.dev/packages/melos/changelog)
* Si usas Intelijj/AndroidStudio:
    * instala el plugin de Flutter (se instala tambien el de DART)
    * activa el soporte de Flutter para el proyecto


1. Descarga el codigo
2. Para eliminar los errores, desde el raiz del proyecto ejecuta:
    ``fvm flutter pub get``
3. Ejecuta: ``melos bootstrap``
3. Para Intellij:
   * En settings -> language & Frameworks -> flutter escoger la ruta $raiz_proyecto/.fvm/flutter_sdk como localizacion de Flutter y Dart
1. Verifica que todo esta en orden:
   1. `melos test`
   3. Ejecuta la aplicación en un emulador [Android/Web]
   4. No te asustes si tarda la vida la **PRIMERA VEZ** que lo ejecutes


## Usage
TODO

## Support
Sube un [ticket](https://gitlab.com/fluttersevilla/kickandflutter/-/issues) o pregunta por [Twitter](https://twitter.com/sevillaflutter)

## Roadmap
Lo primero que queremos hacer es echarlo a andar.

## Contributing
1. Seguimos TDD, asi que hace falta tener tests para todas las funcionalidades que se añadan
1. Escribe el código con el formato que quieras, pero antes de hacer commit asegúrate que:
    * El ancho de la linea sea 200 (no el que viene por defecto de Dart que es 80)
    * Todas listas (widgets, parametros, ...) terminan con ``,`` para que se pongan en columna
1. Todos los commits que hagas tienen el siguiente formato si:
    * Solo estas tu: ``[#NUMERO_DE_ISSUE] descripción de lo que quiere conseguir este commit``
    * Si habeis trabajado en parejas: ``[#NUMERO_DE_ISSUE] (autora/autor/autor) descripción de lo que quiere conseguir este commit``
1. Commitea frecuente y _al turrón_. Si te hace falta create mas issues.
2. Cuando descubras _poyaques_, crea un issue nuevo para investigarlo y sigue con lo que estabas haciendo
2. Rebase antes que Merge que aqui se _cocina_ con [TBD](https://cv.jesuslc.com/assets/slides/2018-11-commitconf-git-y-personas.pdf)
3. Vamos a fijar las dependencias a saco (es decir, no vamos a tirar de **^**)

## Authors and acknowledgment
TODO

## License
Esto tiene licencia [AGPL](https://snyk.io/learn/agpl-license/) 

## Project status
Arrancando y buscando gente que se quiera apuntar

esta no es mi clase 
porque no melllga